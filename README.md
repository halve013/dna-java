# **DNA Program and Script**

The National Center for Biotechnology Information (NCBI) uses the Basic Local Alignment Search Tool (BLAST) to perform comparisons that find the similarities and statistical significance between protein sequences. The purpose of the DNA project is to be able to perform these BLAST comparisons from any machine. To perform this, we created a Java program that uses both Docker and a batch script to acquire protein databases from the NCBI and perform/record BLAST comparisons of the user&#39;s choice.

# **Installation and Download**

Install Docker Desktop on your machine.

1. Download Docker Desktop on your machine from this page: [https://www.docker.com/get-started](https://www.docker.com/get-started)

2. Once the download is complete, run the Installer.

3. When prompted, ensure the Enable Hyper-V Windows Features or the Install required Windows components for WSL 2 option is selected on the Configuration page.

4. Continue following the instructions in the installation wizard. Once the installation is successful, click Close to complete the process.

5. If you are not running Docker on a user account that is not your admin account, you must add the user to the docker-users group. Run Computer Management as an administrator and navigate to Local Users and Groups > Groups > docker-users. Right-click to add the user to the group. Restart your machine for the changes to take effect.

6. Once installation is complete, open Docker Desktop to initialize it.

	Note: If you are on a Windows machine, you may run into an error that looks like this:

   	 	An error occurred 
   	 	Hardware assisted virtualization and data executtion protection must be enabled in the BIOS
    
	If this occurs, you have two options.

	- If you are on a Windows 8 or 10 machine, open a command line with Admin privileges, and enter the following two lines. Once you have finished, restart your machine.
	  - Bcedit /set hypervisorlaunchtype off
	  - Bcedit /set hypervisorlaunchtype auto
	- If you are on a Windows 7 machine, you will need to enable Hardware Virtualization through BIOS. The following link will demonstrate how to do this for a variety of different CPUs.
  	- https://support.bluestacks.com/hc/en-us/articles/360043236951

Install Java JRE onto your machine.

1. Download Java onto your machine from this page: [https://www.java.com/en/download/manual.jsp](https://www.java.com/en/download/manual.jsp)

2. Once the download is complete, run the installer.

3. Follow the instructions in the installation wizard. Once the installation is successful, click Close to complete the process.

Download the Java Program, JDKs, and Script.

1. Either clone the repository from GitHub or download it as a Zip, then extract it to a location of your choice. This will include the Java program without its required JDKs, and the Script used to run the program.

2. Head to https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html, and download a 15.0.2 compressed archive (NOT an installer/Package) based on your operating system. You may need to create an Oracle account to do this. 

3. Once you have downloaded the JDK archive of your choice, open your repository and locate the empty "Java" folder. Then, move the "jdk- 15.0.2" folder from the archive you downloaded into the "Java" folder.

# **Usage**

How to use the DNA script.

 1. Ensure Docker Desktop is running on your machine. Look for an icon of a Whale in your system tray if you are not sure. If it is not there, be sure to open Docker Desktop.

 2. Run Script.bat.
 
 3. The script will first ask for what file path you would like to use. It is recommended that you use the file path the script is saved in, which will be printed at the top of the command line window. The script will then create 6 folders: &quot;blastdb&quot;, &quot;blastdb\_custom&quot;, &quot;fasta&quot;, &quot;queries&quot;, and &quot;results&quot; at the designated location.

 4. The script will then ask what you would like to name the output file.

 5. Next, the script will show a list of every usable database and will ask you to enter the one you would like to use. If you do not already have the database downloaded, the program will download it for you. If you do have the database downloaded already, the script will ask if you would like to update the database. (swissprot and pdb are the best databases to keep search times lower)

 6. The script will now ask if you would like to use a Protein ID or a Sequence to continue. Once you choose which to use, enter the ID/Sequence.

 7. Once you reach this step, the program will download and unzip the contents of the database archive. When it is done, an output file with the name you entered at Step 4 will appear in the &quot;results&quot; folder.

 8. You will then receive a prompt to enter your first signature. Once you have entered that signature, the program will ask you for a second signature.

 9. Once both signatures have been entered, the program will perform the comparisons between them and output the results.
 
 10. The output with the custom calculations will be added to the results folder.

# **Contributors**

- Ivan Zarate
- Kyle Dalton
- Pierre Vasquez
- Lorenc Gasparov
