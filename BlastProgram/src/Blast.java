/**
 * Name: Kyle Dalton, Pierre Vasquez, Ivan Zarate, Lorenc Gasparov
 * Project Name: DNA Protein for Dr Barber at UW - Parkside
 * This program is intended for the use of students attending UWP started by Dr. Barber.
 * This program is built off the script which is included here which submits the actual
 * query via NCBI utilizing Docker. This is the next step to that program where the calculations
 * are done to provide a new metric that gives the user a way to see how similar a protein sequence.
 *
 * Bugs: Calculations are still off, most likely due to the positioning
 * TODO: Allow partial sequences not just off protein id's for sequences, Begin 3D GUI
 */

// Imports
import org.json.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.*;

/**
 * Main Blast class
 */
public class Blast {
    /**
     * Max score sequence 1 can have
     */
    private static double maxScoreOne = 0.0;
    /**
     * Max score sequence 2 can have
     */
    private static double maxScoreTwo = 0.0;
    /**
     * Used to accuses the blossom array by the correct index
     */
    private static final HashMap<Character, Integer> happy = new HashMap<>();

    /**
     * Main
     * @param args Main
     * @throws IOException Exception
     * @throws InterruptedException Exception
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        Blast(args[0], args[1]);
    }

    /**
     * Main function that handles the brunt of the program
     * takes the users info and begins comparing them to
     * @param filePath file path
     * @param fileName file name
     * @throws IOException ex
     * @throws InterruptedException ex
     */
    public static void Blast(String filePath, String fileName) throws IOException, InterruptedException {
        setMap();
        Scanner input = new Scanner(System.in);

        System.out.print("Type in first signature: ");
        String sigOne = input.nextLine();
        System.out.println("First signature is " + sigOne + "\n");

        System.out.print("Type in second signature: ");
        String sigTwo = input.nextLine();
        System.out.println("Second signature is " + sigTwo + "\n");

        // Calculates max score of both signatures inputted by user
        maxScoreOne = maxScore(sigOne);
        maxScoreTwo = maxScore(sigTwo);
        String resultFilePath = (filePath + "\\results\\" + fileName);
        parseJsonFile(resultFilePath + ".json", sigOne, sigTwo, resultFilePath);
    }


    /**
     *  This parses the json file. It prints the ID number out of 100 genes,
     *  prints the evalue, and the gene sequence. Print statements used to keep track
     *  of how the JSON file is parsed.
     *  @param path - file path to the json file
     *  @param sigOneParam - signature one
     *  @param sigTwoParam - signature two
     *  @param resultPath - file path to the results folder
     */
    private static void parseJsonFile(String path, String sigOneParam, String sigTwoParam, String resultPath) {
        // Variables used to hold data
        int number = 0;
        int queryStart = 0;
        int subjectStart = 0;
        int queryEnd = 0;
        int subjectEnd = 0;
        double evalue;
        String query;
        String subject;
        String midline;
        String sciname;

        // Creating the calculations file in the results folder
        try {
            File myObject = new File(resultPath + "Calculations.txt");
            if (myObject.createNewFile()) {
                System.out.println("File has been created");
            } else {
                System.out.println("File already exists");
            }
        } catch (Exception q) {
            System.out.println("An error has occurred " + q + "\n");
        }

        try {
            String contents = new String((Files.readAllBytes(Paths.get(path))));

            // Access everything in the file
            JSONObject BlastOutput = new JSONObject(contents);

            // Access everything inside "BlastOutput2"
            JSONArray blastArray = BlastOutput.getJSONArray("BlastOutput2");

            // Turn everything inside BlastOutput2 into a JSON object
            JSONObject report = blastArray.getJSONObject(0);

            // Access everything inside "report"
            JSONObject results = report.getJSONObject("report");

            // Access everything inside "results"
            JSONObject search = results.getJSONObject("results");

            // Access everything inside "search"
            JSONObject hits = search.getJSONObject("search");

            // Access everything inside the array named "hits"
            JSONArray hitsArray = hits.getJSONArray("hits");


            //  Controls how many sequences you want to check or compare.
            //  You can start from anywhere as long as you tell it where it starts and where it ends in the for loop.
            for(int i = 0; i < hitsArray.length(); i++)    {
                JSONObject findSequences = hitsArray.getJSONObject(i);

                //  Tells you which result number it is. The higher the number the less likely it is
                //  to be the one you are looking for
                number = findSequences.getInt("num");

                //  Collecting description
                //  Description of the result including scientific name
                JSONArray descriptionArray = findSequences.getJSONArray("description");
                JSONObject description = descriptionArray.getJSONObject(0);
                sciname = description.getString("sciname");

                // Access everything inside "hsps"
                JSONArray sequences = findSequences.getJSONArray("hsps");

                // Collect sequences and evalue
                JSONObject geneSequences = sequences.getJSONObject(0);
                queryStart = geneSequences.getInt("query_from");
                subjectStart = geneSequences.getInt("hit_from");
                queryEnd = geneSequences.getInt("query_to");
                subjectEnd = geneSequences.getInt("hit_to");
                query = geneSequences.getString("qseq");
                subject = geneSequences.getString("hseq");
                midline = geneSequences.getString("midline");
                evalue = geneSequences.getDouble("evalue");

                // Find signature scores by comparing
                // System.out.println(queryStart + " " + queryEnd + " " + subjectStart + " " + subjectEnd);
                double sigOneScore = comparison(sigOneParam, query, subject, queryStart, subjectStart, queryEnd, subjectEnd) / maxScoreOne;
                double sigTwoScore = comparison(sigTwoParam, query, subject, queryStart, subjectStart, queryEnd, subjectEnd) / maxScoreTwo;

                System.out.println("Result number: " + number);
                System.out.println("Scientific Name: " + sciname);
                System.out.println("Signature 1 Score: " + sigOneScore + "               " + "Signature 2 Score: " + sigTwoScore);
                System.out.println("Evalue:  " + evalue + "\n" + "Query:   " + query + "\n" + "Midline: " + midline + "\n" + "Subject: " + subject + "\n");

                // Call writeToFile to write data onto file in results folder
                String fileResults = sigOneScore + "                " + sigTwoScore + "\n";
                String sequenceResults = (evalue + "\n");
                writeToFile(resultPath, fileResults, sequenceResults);
            }
        }
        catch (IOException | JSONException e)   {
            e.printStackTrace();
        }
    }

    /**
     * This is the matrix that gives letters their value
     */
    public static final int[][] blosum62Matrix = new int[][]
            {
                    /*
        A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  *  -     */
/*A*/ { 4,-2, 0,-2,-1,-2, 0,-2,-1,-4,-1,-1,-1,-2,-4,-1,-1,-1, 1, 0, 0, 0,-3, 0,-2,-1,-4,-4},
/*B*/ {-2, 4,-3, 4, 1,-3,-1, 0,-3,-4, 0,-4,-3, 3,-4,-2, 0,-1, 0,-1,-3,-3,-4,-1,-3, 1,-4,-4},
/*C*/ { 0,-3, 9,-3,-4,-2,-3,-3,-1,-4,-3,-1,-1,-3,-4,-3,-3,-3,-1,-1, 9,-1,-2,-2,-2,-3,-4,-4},
/*D*/ {-2, 4,-3, 6, 2,-3,-1,-1,-3,-4,-1,-4,-3, 1,-4,-1, 0,-2, 0,-1,-3,-3,-4,-1,-3, 1,-4,-4},
/*E*/ {-1, 1,-4, 2, 5,-3,-2, 0,-3,-4, 1,-3,-2, 0,-4,-1, 2, 0, 0,-1,-4,-2,-3,-1,-2, 4,-4,-4},
/*F*/ {-2,-3,-2,-3,-3, 6,-3,-1, 0,-4,-3, 0, 0,-3,-4,-4,-3,-3,-2,-2,-2,-1, 1,-1, 3,-3,-4,-4},
/*G*/ { 0,-1,-3,-1,-2,-3, 6,-2,-4,-4,-2,-4,-3, 0,-4,-2,-2,-2, 0,-2,-3,-3,-2,-1,-3,-2,-4,-4},
/*H*/ {-2, 0,-3,-1, 0,-1,-2, 8,-3,-4,-1,-3,-2, 1,-4,-2, 0, 0,-1,-2,-3,-3,-2,-1, 2, 0,-4,-4},
/*I*/ {-1,-3,-1,-3,-3, 0,-4,-3, 4,-4,-3, 2, 1,-3,-4,-3,-3,-3,-2,-1,-1, 3,-3,-1,-1,-3,-4,-4},
/*J*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4},
/*K*/ {-1, 0,-3,-1, 1,-3,-2,-1,-3,-4, 5,-2,-1, 0,-4,-1, 1, 2, 0,-1,-3,-2,-3,-1,-2, 1,-4,-4},
/*L*/ {-1,-4,-1,-4,-3, 0,-4,-3, 2,-4,-2, 4, 2,-3,-4,-3,-2,-2,-2,-1,-1, 1,-2,-1,-1,-3,-4,-4},
/*M*/ {-1,-3,-1,-3,-2, 0,-3,-2, 1,-4,-1, 2, 5,-2,-4,-2, 0,-1,-1,-1,-1, 1,-1,-1,-1,-1,-4,-4},
/*N*/ {-2, 3,-3, 1, 0,-3, 0, 1,-3,-4, 0,-3,-2, 6,-4,-2, 0, 0, 1, 0,-3,-3,-4,-1,-2, 0,-4,-4},
/*O*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4},
/*P*/ {-1,-2,-3,-1,-1,-4,-2,-2,-3,-4,-1,-3,-2,-2,-4, 7,-1,-2,-1,-1,-3,-2,-4,-2,-3,-1,-4,-4},
/*Q*/ {-1, 0,-3, 0, 2,-3,-2, 0,-3,-4, 1,-2, 0, 0,-4,-1, 5, 1, 0,-1,-3,-2,-2,-1,-1, 3,-4,-4},
/*R*/ {-1,-1,-3,-2, 0,-3,-2, 0,-3,-4, 2,-2,-1, 0,-4,-2, 1, 5,-1,-1,-3,-3,-3,-1,-2, 0,-4,-4},
/*S*/ { 1, 0,-1, 0, 0,-2, 0,-1,-2,-4, 0,-2,-1, 1,-4,-1, 0,-1, 4, 1,-1,-2,-3, 0,-2, 0,-4,-4},
/*T*/ { 0,-1,-1,-1,-1,-2,-2,-2,-1,-4,-1,-1,-1, 0,-4,-1,-1,-1, 1, 5,-1, 0,-2, 0,-2,-1,-4,-4},
/*U*/ { 0,-3, 9,-3,-4,-2,-3,-3,-1,-4,-3,-1,-1,-3,-4,-3,-3,-3,-1,-1, 9,-1,-2,-2,-2,-3,-4,-4},
/*V*/ { 0,-3,-1,-3,-2,-1,-3,-3, 3,-4,-2, 1, 1,-3,-4,-2,-2,-3,-2, 0,-1, 4,-3,-1,-1,-2,-4,-4},
/*W*/ {-3,-4,-2,-4,-3, 1,-2,-2,-3,-4,-3,-2,-1,-4,-4,-4,-2,-3,-3,-2,-2,-3,11,-2, 2,-3,-4,-4},
/*X*/ { 0,-1,-2,-1,-1,-1,-1,-1,-1,-4,-1,-1,-1,-1,-4,-2,-1,-1, 0, 0,-2,-1,-2,-1,-1,-1,-4,-4},
/*Y*/ {-2,-3,-2,-3,-2, 3,-3, 2,-1,-4,-2,-1,-1,-2,-4,-3,-1,-2,-2,-2,-2,-1, 2,-1, 7,-2,-4,-4},
/*Z*/ {-1, 1,-3, 1, 4,-3,-2, 0,-3,-4, 1,-3,-1, 0,-4,-1, 3, 0, 0,-1,-3,-2,-3,-1,-2, 4,-4,-4},
/***/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1,-4},
/*-*/ {-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4, 1},
            };

    public static void setMap()    {
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ*-";
        for(int x = 0; x < letters.length(); x++) {
            happy.put(letters.charAt(x), x);
        }
    }

    /**
     * This is the method that calculates the max score needed for the end number
     * @param signature signature
     * @return max score
     */
    public static double maxScore(String signature)    {
        double maximum = 0.0;
        String [] splitIt = signature.split("\\s+");
        char letter;

        //  Get the letter of each amino acid in the signature
        for(int x = 0; x < splitIt.length; x++) {
            letter = splitIt[x].charAt(0);

            //  Find the score of each amino acid within the blosum62Matrix and add to maximum
            maximum += (double) blosum62Matrix[happy.get(letter)][happy.get(letter)];
        }
        return maximum;
    }

    /**
     * Writes data onto file created in results folder. The user names the file.
     * Data written is both signature scores and evalue for the result
     * @param filePath - file path
     * @param results - holds the signature scores
     * @param valueString - holds the evalue
     */
    public static void writeToFile(String filePath, String results, String valueString) {

        try {
            FileWriter myWriter = new FileWriter(filePath + "Calculations.txt",true);
            BufferedWriter bw = new BufferedWriter(myWriter);
            bw.write(results);
            bw.write(valueString);
            bw.close();
        }
        catch (Exception e){
            System.out.println("Error in writing results to file " + e);
        }
    }
    /**
     * Compares specific positions between query sequence and subject sequence. A signature
     * will give us specific amino acid to check at a certain position in query sequence.
     * Subject position will be different and depend on how many hyphens query string has from the start to the
     * position and how many hyphens subject string has from the start to the query position.
     * @param signature - the signature
     * @param query - query sequence
     * @param subject - subject sequence
     * @param queryStart - query starting position
     * @param subjectStart - subject starting position
     * @param queryEnd - query ending position
     * @param subjectEnd - subject ending position
     * @return - the score of the complete signature
     */
    public static double comparison(String signature, String query, String subject, int queryStart, int subjectStart, int queryEnd, int subjectEnd)    {
        // Grabbing the numbers in the signature, not letters, inputted by user and putting them in an array
        String [] splitOne = signature.split("\\s+");
        double score = 0.0;
        int origPosition = 0;
        int position = 0;
        int subjectPosition = 0;
        int hyphenQuery = 0;
        int hyphenSubject = 0;
        char queryChar;
        char subjectChar = '*';
        int startDifference = queryStart - subjectStart;
        int queryPosition = 0;

        // For each element in array get a signature score
        for(int x = 0; x < splitOne.length; x++) {
            hyphenQuery = 0;
            hyphenSubject = 0;

            //  Signature position number will be turned from String to integer
            //  and store original position from signature into a temp variable
            position = Integer.parseInt(splitOne[x].substring(1));

            // Saving position as query start
            queryPosition = position;
            origPosition = position;

            // If statement to check whether position user typed in is within range of sequence
            // If so counts all the hyphens in query string from index 0 to (position - 1)
            // If not then query char would be '*'
            if (position >= queryStart && position <= queryEnd) {
                // Actual position in the query string
                position -= queryStart;
                for (int y = 0; y <= position; y++) {
                    if (query.charAt(y) == '-') {
                        hyphenQuery++;
                        position++;
                    }
                }
                queryChar = query.charAt(position);
            }
            else {
                queryChar = '*';
            }

            // Resetting position to original position for subject string
            position = origPosition;
            // Actual position in the subject string
            position -= subjectStart;

            // Count all the hyphens in subject string from index 0 to position
            for (int z = 0; z <= position; z++) {
                if (subject.charAt(z) == '-') {
                    hyphenSubject++;
                    position++;
                }
            }

            //  Equation to find the position within subject sequence
            subjectPosition = queryPosition + hyphenQuery - hyphenSubject - startDifference;

            // If subject position is within range and positive
            // If not then subject character would be '*'
            if (subjectPosition <= subject.length() - 1 && subjectPosition >= 0) {
                if (subjectPosition >= subjectStart && subjectPosition <= subjectEnd) {
                    //  Get character at required position in subject string
                    subjectChar = subject.charAt(subjectPosition);
                }
            }
            else {
                subjectChar = '*';
            }

            //  Get the score for letter in signature and add to total score
            score += blosum62Matrix[happy.get(queryChar)][happy.get(subjectChar)];
        }
        return score;
    }
}