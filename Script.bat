@ECHO OFF

:: Check if required folders are already made
echo Checking if folders already exist
if exist blastdb\ (
  echo Yes, they're all here
) else (
  mkdir blastdb queries fasta results blastdb_custom
)
@echo,

:: Asks for user filepath, prints out current directory in case they want to create everything where they currently are
cd
set /P filepath="What is your file path? "
echo Your filepath is %filepath%

:: Asks for name of file to be saved in variable "filename" which will be used later
@echo,
set /P filename="What do you want to name the file? "
echo Your filename is %filename%
@echo,

:: List all databases available to use
set list=16S_ribosomal_RNA 18S_fungal_sequences 28S_fungal_sequences Betacoronavirus ITS_RefSeq_Fungi ITS_eukaryote_sequences LSU_eukaryote_rRNA LSU_prokaryote_rRNA SSU_eukaryote_rRNA cdd_delta env_nr env_nt human_genome landmark mito mouse_genome nr nt pataa patnt pdbaa pdbnt ref_euk_rep_genomes ref_prok_rep_genomes ref_viroids_rep_genomes ref_viruses_rep_genomes refseq_protein refseq_rna refseq_select_prot refseq_select_rna swissprot taxdb tsa_nr tsa_nt
(for %%a in (%list%) do (
   echo %%a
))

:: Asks user which database they wish to use, downloads it accordingly if they don't have it
@echo,
set /P database="What database do you want to download and or use? They are listed above: "
@echo,
echo Your database is %database%

:: Checks if database already exists and asks if user wants to update, if not then downloads database
if exist blastdb\%database%.tar.gz (
    set /p answer="It seems you already have the database. Do you want to update it? (y/n) "
    ) else (
        :: Downloads database
        docker run --rm -v %filepath%\blastdb:/blast/blastdb:rw -w /blast/blastdb ncbi/blast update_blastdb.pl %database%

        :: Unzips database
        cd %filepath%\blastdb
        tar xzvf %database%.tar.gz
        cd %filepath%
    )

:: Updates database if user input is "y" or does not if input is "n"
if "%answer%"=="y" (
    cd %filepath%\blastdb
    tar xzvf %database%.tar.gz
    cd %filepath%

    goto :output
) else (
    goto :output
)

:output

:: Asks user for what they want to use to blast against and redirects them accordingly
@echo,
set /P queryChoice="Do you want to use a Protein ID(1) or a sequence of letters/FASTA(2) to blast against? "

:: Prints out the user's choice
echo Your have chosen %queryChoice%
@echo,

:: If they chose to use an Accession Number (1) then asks for it and save it in variable "protein" to be used later
:: If they prefer to use a sequence, then sends them to this method and saves it in variable "protein" as well
if "%queryChoice%"=="1" (
    set /P protein="What is your protein ID? "
) else (
    set /P protein="What is your FASTA Sequence? "
    echo Your FASTA Sequence is %protein%
    goto :skipID
)
echo Your protein ID is %protein%

@echo,
:: Retrieve test query
docker run --rm ncbi/blast efetch -db protein -format fasta -id %protein% > queries/%protein%.fsa

:: Uses sequence inputted by user to search against database
:skipID
if "%queryChoice%"=="2" (
    echo Your FASTA Sequence is %protein%
    echo ^>seq1>%filepath%\queries\example.fsa
    echo %protein%>>%filepath%\queries\example.fsa
    goto :fastaBlast
)


:: Runs BLAST+
docker run --rm -v %filepath%\blastdb:/blast/blastdb:ro -v %filepath%\blastdb_custom:/blast/blastdb_custom:ro -v %filepath%\queries:/blast/queries:ro -v %filepath%\results:/blast/results:rw ncbi/blast blastp -query /blast/queries/%protein%.fsa -db %database% -outfmt "15" -out /blast/results/%filename%.json
goto :skipFasta

:fastaBlast
:: Runs BLAST+
docker run --rm -v %filepath%\blastdb:/blast/blastdb:ro -v %filepath%\blastdb_custom:/blast/blastdb_custom:ro -v %filepath%\queries:/blast/queries:ro -v %filepath%\results:/blast/results:rw ncbi/blast blastp -query /blast/queries/example.fsa -db %database% -outfmt "15" -out /blast/results/%filename%.json
goto :skipFasta

:skipFasta
:: Changing directory to where parsing and comparison is
cd BlastProgram\src

::Setting up the jdk for command prompt to use to compile and execute java code
set path=%filepath%\Java\openjdk-15.0.2\bin

:: Compiling program
javac -cp %filepath%\jars\java-json.jar;. Blast.java

:: Run program and pass the filepath and filename to java program
java -cp %filepath%\jars\java-json.jar;. Blast %filepath% %filename%
PAUSE